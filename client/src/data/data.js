export const welcomeSectionData = {
    heading: 'Yo!',
    data: [
        'My name is Jay, I am a JavaScript developer from Brooklyn, New York.',
        'I have over ten years experience in the web development profession, working in multiple fields particularly SaaS and E-Commerce. JavaScript is my passion, I\'ve used it throughout the entire stack and I\'m confident that it is the present and future of not only web development but development in general.'
        ,'I am currently working as a developer at RugsUSA.com but I am available for freelance opportunities, so if you need a JS ninja, get at me bro!'
        ,'Thanks for coming by and checking out my site.'
        ,'Geek specs: M(ongo) E(xpress) R(eact) N(ode) Stack, Flexbox for layout, deployed and hosted on Heroku.'
    ]
};

export const infoSectionData = {
    heading: 'Proficiencies',
    data: [
        'NodeJS',
        'Express',
        'React',
        'Redux',
        'Ember',
        'Mongo',
        'MySQL',
        'ES6',
        'HTML5',
        'CSS3'
    ]
};