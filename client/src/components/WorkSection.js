import React,{Component} from 'react';
import rugsUsaLogo from '../images/rugsusalogo.png';
import qnsLogo from '../images/qnsLogo.jpg';
import ssLogo from '../images/ss.png';
import FontAwesome from 'react-fontawesome';

export default class WorkSection extends Component{
    render(){
        return(
            <div className="work-container">
                <div className="rugs-usa-container">
                    <div className="work-overview">
                        <img src={rugsUsaLogo} alt="rugs usa logo" width="165px" />
                        <h3>RugsUSA.com</h3>
                        <p>2012 - Present</p>
                    </div>
                    <div className="work-details">
                        <h3>Fulltime - Senior Web Developer</h3>
                        <p>Working as a Senior Developer on a large New York based ecommerce website.</p>
                        <p className="accomplish">Selected Accomplishments</p>
                        <ul>
                            <li><FontAwesome className="cog" name="cog" /> Created a 'Utility Belt' application using the MEEN stack for employee use. Streamlined many day to day time consuming functions.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created API using Express to support 3rd party dynamic pricing utility.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created EDI system and API using Express to handle document exchange between trading partners and our ERP software.</li>
                            <li><FontAwesome className="cog" name="cog" /> Ported sister company's website from Wordpress to Shopify.</li>
                            <li><FontAwesome className="cog" name="cog" /> Redesigned website and created responsive solution using Bootstrap.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created customer facing frontend shipment tracking UI with ShipHero.</li>
                        </ul>
                        <p><FontAwesome className="map" name="link" /> <a target="blank" rel="noopener" href="http://www.rugsusa.com/">RugsUSA.com</a></p>
                    </div>
                </div>
                <div className="qns-container">
                    <div className="work-overview">
                        <img src={qnsLogo} alt="qns logo" width="165px" />
                        <h3>Qns.com</h3>
                        <p>2009 - 2012</p>
                    </div>
                    <div className="work-details">
                        <h3>Fulltime - Web Developer</h3>
                        <p>Worked as the sole Web Developer for a New York based local news website.</p>
                        <p className="accomplish">Selected Accomplishments</p>
                        <ul>
                            <li><FontAwesome className="cog" name="cog" /> Integrated MailChimp & Constant Contact to website via API.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created Rails voting app for the site’s Best of the Boro voting competition.</li>
                            <li><FontAwesome className="cog" name="cog" /> Converted legacy site design into responsive mobile ready site using Twitter Bootstrap.</li>
                            <li><FontAwesome className="cog" name="cog" /> Designed daily email blast templates.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created new Wordpress site to replace end of life legacy publishing CMS software.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created script to visualize MySQL data into actionable graphs.</li>
                        </ul>
                        <p><FontAwesome className="map" name="link" /> <a target="blank" rel="noopener" href="http://www.qns.com/">Qns.com</a></p>
                    </div>
                </div>
                <div className="ss-container">
                    <div className="work-overview">
                        <img src={ssLogo} alt="stylesight logo" width="125px" />
                        <h3>Stylesight.com</h3>
                        <p>2007 - 2009</p>
                    </div>
                    <div className="work-details">
                        <h3>Fulltime - Jr. Web Developer</h3>
                        <p>First job out of college, worked as a Jr Web Developer at a New York based fashion trend forecasting company.</p>
                        <p className="accomplish">Selected Accomplishments</p>
                        <ul>
                            <li><FontAwesome className="cog" name="cog" /> Integrated MailChimp & Constant Contact to website via API.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created Rails voting app for the site’s Best of the Boro voting competition.</li>
                            <li><FontAwesome className="cog" name="cog" /> Converted legacy site design into responsive mobile ready site using Twitter Bootstrap.</li>
                            <li><FontAwesome className="cog" name="cog" /> Designed daily email blast templates.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created new Wordpress site to replace end of life legacy publishing CMS software.</li>
                            <li><FontAwesome className="cog" name="cog" /> Created script to visualize MySQL data into actionable graphs.</li>
                        </ul>
                        <p><FontAwesome className="map" name="link" /> <a target="blank" rel="noopener" href="http://www.stylesight.com/">Stylesight.com</a></p>
                    </div>
                </div>
            </div>
        )
    }
}