import React from 'react';
import PropTypes from 'prop-types';

const Section = (props)=>{
    const content = props.data.map((item,index)=>{
        switch(props.elementType){
            default:
                return <p key={index}>{item}</p>
        }
    });
    return(
        <div className={props.cssClass}>
            <h2>{props.heading}</h2>
            {content}
        </div>
    );
};

Section.propTypes = {
    heading: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired
};

export default Section;