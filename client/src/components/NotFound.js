import React from 'react';

const NotFound = (props)=>
    <div className="not-found">
        <h2>404 Error: Page Not Found</h2>
        <p><em>{props.location.pathname}</em> ain't no route I ever heard of... They speak english in <em>{props.location.pathname}</em>?!</p>
    </div>;

export default NotFound;