import React from 'react';
import FontAwesome from 'react-fontawesome';

const Footer = ()=>(
    <footer className="main-footer">
        <div className="footer-inner">
            <span className="social-icons">
                <a href="https://bitbucket.org/jaylane/" target="_blank" rel="noopener noreferrer"><FontAwesome name="bitbucket" size="2x" /></a>
                <a href="https://stackoverflow.com/users/1703854/jay-lane" target="_blank" rel="noopener noreferrer"><FontAwesome name="stack-overflow" size="2x" /></a>
                <a href="https://www.facebook.com/what.is.the.internet" target="_blank" rel="noopener noreferrer"><FontAwesome name="facebook-official" size="2x" /></a>
                <a href="https://twitter.com/jayl_ane" target="_blank" rel="noopener noreferrer"><FontAwesome name="twitter" size="2x" /></a>
                <a href="https://www.instagram.com/weed_dawg420/" target="_blank" rel="noopener noreferrer"><FontAwesome name="instagram" size="2x" /></a>
            </span>
            
            <span className="copyright">&copy;2017 Jay-Lane.com</span>
        </div>
    </footer>
);

export default Footer;