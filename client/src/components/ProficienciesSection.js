import React, {Component} from 'react';
import FontAwesome from 'react-fontawesome';
import vscodeLogo from '../images/vscode.png';
import babelLogo from '../images/babel.png';
import gitLogo from '../images/git.png';
import gulpLogo from '../images/gulp.svg';
import herokuLogo from '../images/heroku.png';
import npmLogo from '../images/npm.png';
import webpackLogo from '../images/webpack.svg';
import jestLogo from '../images/jest.png';
import mochaLogo from '../images/mocha.png';

export default class ProficienciesSection extends Component{
    render(){
        return(
           <div className="prof-section">
                <div className="pro-skills">
                    <h3>Professional Skillset</h3>
                    <ul>
                        <li><FontAwesome className="dot" name="check-square-o" /> JavaScript ES6</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Express & NodeJS</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> React</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Ember</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Behavior driven development</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Unit testing (Mocha & Jest)</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> HTML5</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> CSS3 & Sass</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> MySQL & MongoDB</li>
                    </ul>
                </div>
                <div className="working-knowledge-skills">
                    <h3>Working Knowledge</h3>
                    <ul>
                        <li><FontAwesome className="dot" name="check-square-o" /> Redux</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Amazon Web Services</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Ruby On Rails</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Perl</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Linux Administration</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Apache Web Server</li>
                        <li><FontAwesome className="dot" name="check-square-o" /> Docker</li>
                    </ul>
                </div>
                <div className="tools">
                    <h3>My Toolbox</h3>
                    <div className="tool-images">
                        <img src={vscodeLogo} alt="vscode logo" width="50px"/>
                        <img src={babelLogo} alt="babel logo" width="100px"/>
                        <img src={gitLogo} alt="git logo" width="70px"/>
                        <img src={gulpLogo} alt="gulp logo" width="40px"/>
                        <img src={herokuLogo} alt="heroku logo" width="80px"/>
                        <img src={npmLogo} className="last" alt="npm logo" width="70px"/>
                        <img src={webpackLogo} className="last" alt="webpack logo" width="50px"/>
                        <img src={mochaLogo} className="last" alt="mocha logo" width="60px"/>
                        <img src={jestLogo} className="last" alt="jest logo" width="40px"/>
                    </div>
                </div>
           </div>
        )
    }
}