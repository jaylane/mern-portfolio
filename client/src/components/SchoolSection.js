import React, {Component} from 'react';
import logo from '../images/college.png';

export default class SchoolSection extends Component{
    render(){
        return(
            <div className="school-content">
                <img src={logo} alt="Champlain College" className="collegeLogo" />
                <div className="school-text">
                    <h3 className="school-header">Champlain College, Burlington Vermont</h3>
                    <h4>Graduated: 2007</h4>
                    <p>Received Bachelors Degree in Computer Science with a focus on Digital Forensics.</p>
                    <p><em>Thesis:</em> The threat of Botnets on the physical infrastructure of the United States</p>
                </div>
            </div>
        );
    }
}