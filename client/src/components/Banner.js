import React from 'react';
import logo from '../images/logo2.svg';

const Banner = ()=>(
    <div className="banner">
        <embed className="logo" src={logo} alt="Jay-Lane" />
        <h1 className="headline">Jay Lane</h1>
        <span className="tagline">JavaScript Developer | Brooklyn. NY.</span>          
    </div>
);

export default Banner;