import React from 'react';
import photo from '../images/me.jpg';

const About = ()=>
    <div className="primary col about">
        <img className="photo" src={photo} alt="jay lane" />
        <div className="about-text">
            <h2>About Meh</h2>
            <p>I am 32 years old and a cancer survivor. I had a 3 year long battle with lymphoma which required chemo, radiation, and 2 stem cell transplants. Finally after all that treatment having a stranger's stem cells transplanted into me was the thing that worked. I have been in remission for 2 years now without issue (knock on wood FFS). My battle with cancer taught me a lot about myself and also taught me many valuable lessons, most importantly to never take anything for granted.</p>
            <p>I started creating websites when I was 11 years old on angelfire.com before JavaScript was even released, one of my classic gems is still alive at <a href="http://www.angelfire.com/ny3/outtamywayrecords/" target="_blank" rel="noopener noreferrer">outta my way records</a> (up the punx). I've been a computer geek ever since spending most of my time gaming, developing, and reading endless amounts of documentation.</p>
            <p>Like most developers I am self taught, instead of getting my bachelor's degree in programming I decided to major in digital forensics and computer science at Champlain College in Burlington, Vermont. It was a great program and a lovely place to live, but I kept programming and teaching myself emerging languages and frameworks for fun. When it came time to get a job out of college I decided to develop websites and webapps rather than chase hackers and lock down networks.</p>
            <p>I got my first development position at a SaaS company in the fashion sector called Stylesight, barebones frontend work but I had an incredible mentor as a boss who taught me a lot about both front end and back end development. It was a great learning atmosphere and totally nurtured my love of the internet and programming.</p>
            <p>Over the years since I left Stylesight many things have changed in regards to JavaScript, it morphed into what is now isomorphic JS where I can use my favorite language for the entire stack. When I wrote my first API using Express it made my jaw hit the floor, how easy, clean, and fast you could ship an API. What use to take me months to do with Ruby on Rails, I could finish in a few days.</p>
            <p>Alright I'm sure you've heard me swoon enough about JavaScript...</p>
            <p>I reside in Greenpoint, Brooklyn with my wife Molly and our two cats Archie aka Chicky and Nelson aka Bone. I am a diehard Mets, New York Giants, and Manchester City fan. I spend the majority of my time going to punk and hardcore shows, collecting records, watching movies, and playing video games (if you want a shot at the title in Fifa 18 on ps4, come at me bro. Gamertag: JaxBane).</p>
            <p>If you read all of this, I am impressed.</p>
        </div>
    </div>
;

export default About;