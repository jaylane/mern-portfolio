import React,{Component} from 'react';
import {NavLink} from 'react-router-dom';

export default class Nav extends Component{
    _handleWindowResize = ()=>{
        //set currentWidth and showMobileNav on window resize
        
    }
    toggleNav = ()=>{
        //check the nav wrap to see if it is displaying as block if so, hide the div and update the state
        let navWrap = document.querySelector('.inner-nav-wrap');
        document.querySelector('.burger').classList.toggle('open');
        navWrap.classList.toggle('opens');
        
    }
    render(){
        let burger = <div className="burger" onClick={this.toggleNav}><span></span><span></span><span></span></div>;
        return(
            <nav>
                <ul className="main-nav">
                        { burger }
                        <div className="inner-nav-wrap">
                            <li><NavLink exact to="/about" activeClassName="nav-active">About</NavLink></li>
                            <li><NavLink exact to="/resume" activeClassName="nav-active">Resume</NavLink></li>
                            <li><NavLink exact to="/portfolio" activeClassName="nav-active">Portfolio</NavLink></li>
                            <li><NavLink exact to="/contact" activeClassName="nav-active">Contact</NavLink></li>
                        </div>
                </ul>
            </nav>
        )
    }
};