import React, {Component} from 'react';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import ContactForm from './ContactForm';
import ThankYou from './ThankYou';

export default class Contact extends Component{
    state = {
        showForm:true
    }
    toggleForm = ()=>{
        this.setState({...this.state,showForm: !this.state.showForm});
    }
    render(){
        let body;
        let myKey;
        if(this.state.showForm){
            body = <ContactForm toggleForm={this.toggleForm}/>;
            myKey = 1;
        }else{

            body = <ThankYou />;
            myKey = 2;
        }
        return(
            <div className="form-container">
                <h2>Drop me a line...</h2>
                <TransitionGroup>
                    <CSSTransition key={myKey} classNames="fadeIn" timeout={{enter: 1000}} exit={false} >
                        {body}
                    </CSSTransition>
                </TransitionGroup>
          </div>
        );
    }
}