import React,{Component} from 'react';
import Nav from './Nav';
import {TransitionGroup,CSSTransition} from 'react-transition-group';
import {Link} from 'react-router-dom';

class Header extends Component{
    render(){
        return(
            <header className="main-header">
                <TransitionGroup>
                    <CSSTransition key="name" classNames="slide" timeout={{appear: 1000}} enter={false} exit={false} appear={true}>    
                        <h1 className="name"><Link to="/">Jay Lane</Link></h1>
                    </CSSTransition>    
                </TransitionGroup>
                <TransitionGroup>
                    <CSSTransition key="nav" classNames="slide-right" timeout={{appear: 1000}} enter={false} exit={false} appear={true}>
                        <Nav />
                    </CSSTransition>
                </TransitionGroup>
            </header>
        );
    }
};

export default Header;