import React, {Component} from 'react';
import rugsUsaSite from '../images/rugsUsaSite.png';
import nuloomSite from '../images/nuloomSite.png';
import qnsSite from '../images/qnsSite.png';
import champsSite from '../images/champsSite.png';
import FontAwesome from 'react-fontawesome';

export default class Portfolio extends Component{
    render(){
        return(
            <div className="portfolio-wrap">
                <div className="portfolio-header">
                    <h2>Portfolio</h2>
                </div>
                <div className="portfolio-content">
                    <div className="portfolio-item">
                        <div className="portfolio-item-inner">
                                <div className="card-front">
                                    <img src={rugsUsaSite} alt="Rugs USA homepage"/>
                                </div>
                                <div className="card-back bg-mint">
                                    <p className="title">RugsUSA.com</p>
                                    <p className="desc">Large scale ecommerce website selling rugs and other home furnishings.</p>
                                    <p className="link"><a target="_blank" href="https://www.rugsusa.com" rel="noopener noreferrer"><FontAwesome name="link" /> RugsUSA.com</a></p>
                                </div>   
                        </div> 
                    </div>
                
                    <div className="portfolio-item">
                        <div className="portfolio-item-inner">
                                <div className="card-front">
                                    <img src={nuloomSite} alt="nuLoom homepage"/>
                                </div>
                                <div className="card-back bg-mint">
                                    <p className="title">Nuloom.com</p>
                                    <p className="desc">Sister company of RugsUSA.com sells highend home furnishings.</p>
                                    <p className="link"><a target="_blank" href="https://www.nuloom.com" rel="noopener noreferrer"><FontAwesome name="link" /> Nuloom.com</a></p>
                                </div>   
                        </div>
                    </div>

                    <div className="portfolio-item">
                        <div className="portfolio-item-inner">
                                <div className="card-front">
                                    <img src={qnsSite} alt="Qns homepage"/>
                                </div>
                                <div className="card-back bg-mint">
                                    <p className="title">Qns.com</p>
                                    <p className="desc">Local news website based in Queens, New York.</p>
                                    <p className="link"><a target="_blank" href="http://www.qns.com" rel="noopener noreferrer"><FontAwesome name="link" /> Qns.com</a></p>
                                </div>   
                        </div>
                    </div>
                    <div className="portfolio-item">
                        <div className="portfolio-item-inner">
                                <div className="card-front">
                                    <img src={champsSite} alt="champs homepage"/>
                                </div>
                                <div className="card-back bg-mint">
                                    <p className="title">Champs Diner</p>
                                    <p className="desc">My friend's vegan restaurant in Bushwick,Brooklyn. Currently building a replacement web app with the MERN Stack.</p>
                                    <p className="link"><a href="http://www.champsdiner.com" rel="noopener noreferrer" target="_blank"><FontAwesome name="link" /> ChampsDiner.com</a></p>
                                </div>
                        </div>
                    </div>
                </div>    
            </div>
        )
    }
}