import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import '../css/App.css';
import '../css/font-awesome.min.css';
import Header from './Header';
import Footer from './Footer';
import Banner from './Banner';
import Section from './Section';
import {welcomeSectionData} from '../data/data';
import NotFound from './NotFound';
import About from './About';
import Contact from './Contact';
import Resume from './Resume';
import Portfolio from './Portfolio';
import {CSSTransition, TransitionGroup} from 'react-transition-group';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Header />
          <Banner />
          <div className="row">
          <Route render={({location})=>(
          <TransitionGroup className="trans-wrap">
              <CSSTransition key={location.pathname.split('/')[1]} timeout={{enter:1000, exit: 300}} classNames="fade">
                <Switch location={location}>
                  <Route exact path="/" component={()=> <Section heading={welcomeSectionData.heading} data={welcomeSectionData.data} cssClass="primary col" />} />
                  <Route path="/about" component={About} />
                  <Route path="/contact" component={Contact} />
                  <Route path="/resume" component={Resume} />
                  <Route path="/portfolio" component={Portfolio} />
                  <Route component={NotFound} />
                </Switch>
              </CSSTransition>
              </TransitionGroup>
            )} /> 
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
