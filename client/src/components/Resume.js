import React, {Component} from 'react';
import {NavLink,Route,Redirect} from 'react-router-dom';
import SchoolSection from './SchoolSection';
import ProficienciesSection from './ProficienciesSection';
import WorkSection from './WorkSection';

export default class Resume extends Component{
    render(){
        return(
            <div className="resume-container">
                <h2>Resume</h2>
                <ul className="sub-nav">
                    <li><NavLink activeClassName="nav-active" to={`${this.props.match.url}/school`}>School</NavLink></li>
                    <li><NavLink activeClassName="nav-active" to={`${this.props.match.url}/proficiencies`}>Proficiencies</NavLink></li>
                    <li><NavLink activeClassName="nav-active" to={`${this.props.match.url}/work`}>Work History</NavLink></li>
                </ul>
                <div className="resume-content">
                    <Route exact path={`${this.props.match.path}`} render={()=> <Redirect to={`${this.props.match.path}/school`} />} />
                    <Route path={`${this.props.match.path}/school`} render={()=><SchoolSection />} />
                    <Route path={`${this.props.match.path}/proficiencies`} render={()=><ProficienciesSection />} />
                    <Route path={`${this.props.match.path}/work`} render={()=><WorkSection />}  />
                </div>
            </div>
        )
    }
}