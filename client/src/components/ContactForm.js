import React, {Component} from 'react';
import 'whatwg-fetch';

export default class ContactForm extends Component{
    //handle form submission
    handleSubmit = (e)=>{
        //prevent default form behavior
        e.preventDefault();
        //assign vars from input values
        let name = this.name.value,
        email = this.email.value,
        phone = this.phone.value,
        message = this.message.value;

        //send post request to API endpoint with submission data
        fetch('/api/contact', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              name: name,
              email: email,
              phone: phone,
              message: message
            })
          });
        //clear form on submit
        this.form.reset();
        //hide form display thank you by updating state
        this.props.toggleForm();
    }
    render(){
        return(<form className="contact-form" onSubmit={this.handleSubmit} ref={(form)=> this.form = form}>
            <ul className="flex-outer">
                <li>
                    <label htmlFor="name">Name</label>
                    <input type="text" id="name" placeholder="Enter your name here" required ref={(input)=> this.name = input}/>
                </li>
                <li>
                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" placeholder="Enter your email here" required ref={(input)=> this.email = input}/>
                </li>
                <li>
                    <label htmlFor="phone">Phone</label>
                    <input pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" type="tel" id="phone" placeholder="Enter your phone here" ref={(input)=> this.phone = input} />
                </li>
                <li>
                    <label htmlFor="message">Message</label>
                    <textarea rows="6" id="message" placeholder="Enter your message here" required ref={(input)=> this.message = input}></textarea>
                </li>
                <li>
                    <button type="submit">Submit</button>
                </li>
            </ul>
        </form>)
    }
}