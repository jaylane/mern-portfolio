var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactSchema = new Schema({
    name: String,
    email: String,
    phone: {type: String, default: 'Not Provided'},
    message: String
},{timestamps:true});

module.exports = mongoose.model('Contact', contactSchema);