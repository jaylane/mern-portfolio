var express = require('express');
var router = express.Router();
var api = require('../api/api');

//post route for contact api
router.post('/contact',(req, res, next)=>{
    api.contact(req,res,next);
});

module.exports = router;