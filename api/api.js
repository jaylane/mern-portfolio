var Contact = require('../models/contact');
var helpers = require('../helpers');
var mailgun = require('mailgun-js')({apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_DOMAIN});;

module.exports.contact = (req,res,next)=>{
    //set variables from req body (form input)
    let {name,email,phone,message} = req.body,
    //create new contact document
    contact = new Contact({name,email,phone,message});
    //save contact to mongo database
    contact.save((err)=>{
        //error handling
        if(err){
            console.error('Error during saving contact to database: '+err);
            next(err);
        }
        console.log('saved contact info to database');
    });
    //create email user helper method
    var createdEmail = helpers.createEmail(name,email,phone,message);
    //send email via gmail-send
    var data = {
        from: `${name} <${email}>`,
        to: 'jason.j.lane@gmail.com',
        subject: 'Contact Form Submission from Jay-Lane.com',
        text: `Contact form submission from Jay-Lane.com \nName: ${name} \nEmail: ${email}\n Phone: ${phone}\n Message: ${message}`,
        html: createdEmail
      };
    
    mailgun.messages().send(data, function (err, body) {
        if(err){
            console.error(err);
            next(err);
        }
        console.log(`response from mailgun: ${body.message}`);
        res.staus = 200;
        res.send('Email Sent Successfully');
    });
}